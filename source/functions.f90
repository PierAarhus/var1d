   module int_arg
   contains
     real*8 function integrand(i,j,length,center,x)
     implicit none
     integer :: i,j
     real*8 :: x,length,center,V_pt,eigenfn_i,eigenfn_j,res
     real*8, parameter :: pi=acos(-1.00d+00)


     V_pt=2.0*x**2.0

     eigenfn_i=sqrt(2.0d+00/length)*sin((real(i)*pi*(x-center+length/2.0d+0))/length)
     eigenfn_j=sqrt(2.0d+00/length)*sin((real(j)*pi*(x-center+length/2.0d+0))/length)
                                         
     integrand=eigenfn_i*V_pt*eigenfn_j
     end function

     real*8 function wf_value(H,bs_fn,state,length,center,wf_pt,x)
     integer :: i,j,state,wf_pt,bs_fn
     real*8 :: x,length,center
     real*8, dimension(bs_fn) :: eigenfn
     real*8, dimension(bs_fn,bs_fn) :: H  
     real*8, parameter :: pi=acos(-1.0d+00)     

     do i=1,bs_fn
        eigenfn(i)=sqrt(2.0d+00/length)*sin((real(i)*pi*(x-center+length/2.0d+00))/length)
     enddo

     wf_value=0.0d+00
     do i=1,bs_fn
        wf_value=wf_value+H(i,state)*eigenfn(i)
     enddo


     end function
  end module   
















   
