module inp_out
contains

subroutine read_inp(inp_file,mass,center,length,nbs,int_pt,wfn,density,&
           eigenst_wfn,eigenst_dns)
implicit none
character (len=100) :: inp_file,line
integer, parameter :: default_pt=10000, default_nbs=100
real*8 :: center,length,mass
integer :: nbs,ios,i,int_pt,eigenst_wfn,eigenst_dns
logical :: wfn, density

open(1, file=inp_file, status='old',iostat=ios)
if (ios /=0) stop "Error opening input file."
i=0
do while (i==0)
   read(1,'(a100)') line
   if (line(1:4)=='MASS') i=1
   end do
read(1,*) mass
close(1)

open(1, file=inp_file, status='old',iostat=ios)
if (ios /=0) stop "Error opening input file."
i=0
do while (i==0)
   read(1,'(a100)') line
   if (line(1:6)=='LENGTH') i=1
   end do
read(1,*) length
close(1)


int_pt=default_pt
open(1, file=inp_file, status='old',iostat=ios)
if (ios /=0) stop "Error opening input file."
i=0
do while (i==0)
   read(1,'(a100)') line
   if (line(1:11)=='INTEGRATION') i=1
   if (line(1:3)=='END') i=2
end do
if(i==1) read(1,*) int_pt
close(1)


open(1, file=inp_file, status='old',iostat=ios)
if (ios /=0) stop "Error opening input file."
i=0
do while (i==0)
   read(1,'(a100)') line
   if (line(1:6)=='CENTER') i=1
   end do
read(1,*) center
close(1)

nbs=default_nbs
open(1, file=inp_file, status='old',iostat=ios)
if (ios /=0) stop "Error opening input file."
i=0
do while (i==0)
   read(1,'(a100)') line
   if (line(1:5)=='BASIS') i=1
   if (line(1:3)=='END') i=2
   end do
if (i==1) read(1,*) nbs
close(1)

wfn=.false.
open(1, file=inp_file, status='old',iostat=ios)
if (ios /=0) stop "Error opening input file."
i=0
do while (i==0)
   read(1,'(a100)') line
   if (line(1:5)=='WFN') i=1
   if (line(1:3)=='END') i=2
   end do
if (i==1) then
  wfn=.true.
  eigenst_wfn=1
  read(1,*) eigenst_wfn
endif
close(1)


density=.false.
open(1, file=inp_file, status='old',iostat=ios)
if (ios /=0) stop "Error opening input file."
i=0
do while (i==0)
   read(1,'(a100)') line
   if (line(1:4)=='DENS') i=1
   if (line(1:3)=='END') i=2
   end do
if (i==1) then
  density=.true.
  read(1,*) eigenst_dns
endif
close(1)

end subroutine

subroutine write_out(file_name,out_file,int_time,diag_time,nbs,num_pt,length,center,eigenval,virial,wfn,density)
implicit none

integer:: nbs,i,num_pt
character(len=100)::out_file,file_name,wfn_ext='.wfn',dens_ext='.dns'
real*8::int_time, diag_time,length,center
real*8,dimension(nbs)::virial,eigenval
logical :: wfn,density

open(2,file=out_file,status='replace')
write(2,'(A)') 'CONGRATULATIONS! YOU HAVE JUST SOLVED THE ONE DIMENSIONAL SCHRODINGER EQUATION!'
write(2,'(A)') 'BELOW YOU FIND SOME RESULTS FROM YOUR CALCULATION.'
write(2,'(A)') ' '
write(2,'(A)') ' '
write(2,'(A,f8.5)') 'BASIS SET GENERATED FROM A BOX WITH LENGTH (A.U.):  ',length
write(2,'(A)') ' '                                                          
write(2,'(A,I6)') 'NUMBER OF BASIS SET FUNCTIONS:  ',nbs
write(2,'(A)') ' '
write(2,'(A,f8.5)') 'BOX CENTER (A.U.):  ',center
write(2,'(A)') ' '                      
write(2,'(A,I6)') 'GRID POINTS FOR THE NUMERICAL INTEGRATION:  ',num_pt                               
write(2,'(A)') ' '  
write(2,'(A,f8.5)') 'NUMERICAL INTEGRATION, TIME (S):   ',int_time 
write(2,'(A)') ' '
write(2,'(A,f8.5)') 'HAMILTONIAN MATRIX DIAGONALIZATION, TIME (S):   ',diag_time 
write(2,'(A)') ' '
write(2,'(A)') '___________________________________________________________________________________________________________________'
write(2,'(A)') ' '
write(2,'(A)') 'EIGENSTATE:       1                2                3                4                5                6           '
write(2,'(A)') '__________________________________________________________________________________________________________________ '
write(2,'(A)') ' '
write(2,'(A,f14.5,f17.5,f17.5,f17.5,f17.5,f17.5)') 'E(a.u.)',eigenval(1:6)
write(2,'(A,f17.5,f17.5,f17.5,f17.5,f17.5,f17.5)') 'V/T ',virial(1:6)
write(2,'(A)') '__________________________________________________________________________________________________________________ '
write(2,'(A)') ' '
if(wfn.eqv..true.) write(2,'(A,A,A)') 'WAVEFUNCTION FILE CREATED UNDER REQUEST AS : ',trim(file_name)//wfn_ext
if(density.eqv..true.) write(2,'(A,A,A)') 'DENSITY FILE CREATED UNDER REQUEST AS: ',trim(file_name)//dens_ext


close(2)

end subroutine



end module
