   program PIB
   use :: int_arg
   use :: inp_out
   implicit none
   real*8, parameter :: pi=acos(-1.0d+00),shift=1.0d-08
   integer, parameter :: LWMAX=1000,wf_pt=5000
   real*8, allocatable, dimension(:,:) :: V,integr,H
   real*8, allocatable, dimension(:) :: eigenfn_upt, eigenval_upt, W,kin_exval,vir_rat
   integer :: state_wfn, state_dns !engenstate to plot in wfn and dns files
   character(len=100) :: file_name,inp_ext='.inp',out_ext='.out', wfn_ext=".wfn",dens_ext=".dns"
   character(len=100) :: inp_file , out_file,line_out
   integer :: INFO,LWORK,i,j,k,l,m,bs_fn,num_pt
   real*8:: length,center,mass,x, pt_pot,eigenfn_ref,start,finish,int_time, diag_time
   real*8 :: WORK(LWMAX)
   logical :: wfn,density

   i=1
   call get_command_argument(i, file_name)
   inp_file=trim(file_name)//trim(inp_ext)
   out_file=trim(file_name)//trim(out_ext)


    
   call read_inp(inp_file,mass,center,length,bs_fn,num_pt,wfn,density,state_wfn, state_dns)
   center=center+shift

   allocate(V(bs_fn,bs_fn))
   allocate(integr(bs_fn,bs_fn))
   allocate(H(bs_fn,bs_fn))
   allocate(eigenfn_upt(bs_fn))
   allocate(eigenval_upt(bs_fn))
   allocate(W(bs_fn))
   allocate(kin_exval(bs_fn))
   allocate(vir_rat(bs_fn))

!  UNPERTURBED PIB EIGENVALUES

   do i=1,bs_fn
      eigenval_upt(i)=(((real(i))**2.0d+00)*(pi**2.0d+00))/(2.0d+00*mass*length**2.0d+00)
   enddo

!  MATRIX REPRESENTATION OF THE PERTURBATION POTENTIAL

   call cpu_time(start)
   do i=1,bs_fn
         V(i,i)=0.0d+00
         do j=0,num_pt
            x=(center-length/2.0d+0)+(length/real(num_pt))*real(j) 
            integr(i,i)=integrand(i,i,length,center,x)
            if (j.eq.0.or.j.eq.num_pt) then
               V(i,i)=V(i,i)+integr(i,i)
            else
               V(i,i)=V(i,i)+2.0d+0*integr(i,i)
            endif
         enddo
   enddo 



   do i=1,bs_fn
      do k=i+1,bs_fn
         V(i,k)=0.0d+00
         do j=0,num_pt
            x=(center-length/2.0d+0)+(length/real(num_pt))*real(j)
            integr(i,k)=integrand(i,k,length,center,x)
            if (j.eq.0.or.j.eq.num_pt) then
               V(i,k)=V(i,k)+integr(i,k)
            else
               V(i,k)=V(i,k)+2.0d+00*integr(i,k)
            endif
         enddo
      enddo
   enddo

   do i=1,bs_fn
      do k=1,bs_fn
         V(k,i)=V(i,k)
      enddo
   enddo
 V=(length/(2.0d+00*num_pt))*V

call cpu_time(finish)
int_time=finish-start

!   #####################################################
!   #######      DIAGONALIZATION SECTION          #######
!   #####################################################

!   FULL HAMILTONIAN MATRIX DEFINITION: it consists in the matrix V where on the diagonal the eigenvalues (upt) are added      

    H=V
    do i=1,bs_fn
       H(i,i)=H(i,i)+eigenval_upt(i)
    enddo

! EIGENDECOMPOSITION OF THE HAMILTONIAN: lapack subroutine (dsyev) for sym. mtx diagonalization used

   CALL CPU_TIME(start)

   LWORK = -1
   call dsyev('V','U',bs_fn,H,bs_fn,W,WORK,LWORK,INFO)
   LWORK = min( LWMAX, int( WORK( 1 ) ) )
   call dsyev('V','U',bs_fn,H,bs_fn,W,WORK,LWORK,INFO)
   if( INFO.GT.0 ) then
         write(*,*)'The algorithm failed to compute eigenvalues.'
         stop
   end if

  call cpu_time(finish)

  diag_time= finish - start

!  KINETIC ENERGY EXTRACTION FROM THE EIGENVECTORS
   kin_exval=0.0d+0
   do i=1,bs_fn
      do j=1,bs_fn
         kin_exval(i)=kin_exval(i)+(H(j,i)**2.0d+0)*eigenval_upt(j)      
      enddo
   enddo
   vir_rat=0.0d+0
   vir_rat=(W-kin_exval)/kin_exval


!    PLOT OF THE WAVEFUNCTION
   if(wfn.eqv..true.) then
     x=0.0d+00
     open(3, file=trim(file_name)//wfn_ext, status='replace')
     do i=1,wf_pt 
        x=center-length/2.0d+00+((length)/real(wf_pt))*real(i)
        write(3,'(2e16.8)') x,(wf_value(H,bs_fn,state_wfn,length,center,wf_pt,x))
     enddo
     close(3)
    endif

  if(density.eqv..true.) then  
    x=0.0d+00
    open(4, file=trim(file_name)//dens_ext, status='replace')
    do i=1,wf_pt
       x=center-length/2.0d+00+((length)/real(wf_pt))*real(i)
       write(4,'(2e16.8)') x,((wf_value(H,bs_fn,state_dns,length,center,wf_pt,x))**2.0)
    enddo
    close(4)
  endif


  call write_out(file_name,out_file,int_time,diag_time,bs_fn,num_pt,length,center,W,vir_rat,wfn,density)

  end program



