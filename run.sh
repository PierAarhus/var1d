#!/bin/bash

#----------------------------------------------------------------------------------------
# PATH TO THE SOURCE DIRECTORY TO BE MODIFIED BY THE USER
#----------------------------------------------------------------------------------------																						
PATH_src='/your_path/Var1D/source/'

#----------------------------------------------------------------------------------------

#Check on the input file specification!!!
if [ -z "$1" ] ; then
echo " No input file specified! "
exit
fi



MY_PATH=`pwd`
inp_file=$1
templ_file='templ_functions'
module_int='functions.f90'
compiler='gfortran'
libraries='-llapack'
main='PIB.f90'
execut='PIB.exe'
mod_1='functions.f90'
mod_2='inp_out.f90'
potential=`grep -A1 'POTENTIAL' ${inp_file}.inp | tail -n 1 ` 
replace='(func)'

cd $PATH_src

sed "s#(func)#${potential}#" ${templ_file} > $module_int

$compiler $mod_1 $mod_2 $main $libraries -o $execut

cp $execut $MY_PATH

cd $MY_PATH

./$execut $1

rm $execut
